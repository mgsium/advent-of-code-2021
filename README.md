### Advent of Code 2021

Advent of Code is a an advent calendar of small programming puzzles.

This repository contains my solutions to the 2021 Advent of Code puzzles, written in Haskell.

Learn more about Advent of Code at <https://adventofcode.com/2021/about>
