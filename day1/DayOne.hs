{-# LANGUAGE TemplateHaskell #-}

module DayOne where

import Data.FileEmbed (embedStringFile)

-- Load the list of numbers from the input file
numbers :: [Int]
numbers = map read $ lines $(embedStringFile "assets/input")

-- Count the number of increases between consective days
count :: Int
count = sum [1 | (x, y) <- zip d (tail d), y > x]
		where d = numbers
