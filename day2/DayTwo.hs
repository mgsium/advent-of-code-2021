{-# LANGUAGE TemplateHaskell, LambdaCase #-} 

module DayTwo where

import Data.FileEmbed 	(embedStringFile)
import Data.List	(mapAccumL)
import Data.Maybe

-- Data type to store depth and length data
data Translate = Forward Int | Down Int
		 deriving (Show)

-- Smart constructor
toTranslate :: String -> Maybe Translate
toTranslate s = \case {
			"forward" -> return $ Forward d; 
			"down" -> return $ Down d; 
			"up" -> return $ Down (negate d);
			_ -> Nothing 
		} $ c
		where (c:(n:_)) = words s
		      d = read n

-- Load the input data from the input file
commands :: [Translate]
commands = catMaybes $ toTranslate <$> lines $(embedStringFile "assets/input.txt")

-- Find the product of the final depth and horizontal position
partOne :: Int
partOne = depth * horizontalPos 
		where depth = sum [x | (Down x) <- commands]
		      horizontalPos = sum [x | (Forward x) <- commands]

-- Same as above, but update the aim according to the rules 
partTwo :: Int
partTwo = (\(_, x, y) -> x*y) $ foldl f (0, 0, 0) commands
		where f (aim, h, d) (Down x) = (aim+x, h, d) 
		      f (aim, h, d) (Forward x) = (aim, h+x, d+(x*aim))
