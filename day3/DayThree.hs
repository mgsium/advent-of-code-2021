{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}

module DayThree where

import Data.FileEmbed    (embedStringFile)
import Data.Function	 (on)
import Data.List
import Data.Ord		(comparing)

entries :: [String]
entries = lines $(embedStringFile "assets/input.txt")

toDelta = \case { '1' -> 1; _ -> -1 }

toBit :: (Ord a, Num a, Num p) => a -> p
toBit x | x < 0 = 0
toBit _ = 1

calculateMode :: [String] -> [Int]
calculateMode = map (toBit . sum) . transpose . map (map toDelta)

entryModes :: [Int]
entryModes = calculateMode entries

partOne = (\(g, e, _) -> g * e) $ foldl' f (0, 0, 2 ^ 11) entryModes
            where f (g, e, m) 1 = (g + m, e, m `div` 2) 
                  f (g, e, m) _ = (g, e + m, m `div` 2)

filterEntries :: Int -> (Char -> Bool) -> [String] -> [String]
filterEntries bitPos f = filter (f . (!! bitPos))

mostCommonValue :: Int -> [String] -> Char
mostCommonValue bitPos = f . mostCommonValue' bitPos
				where f (True, _) = '1'
			      	      f (_, x) = x

mostCommonValue' :: Int -> [String] -> (Bool, Char)
mostCommonValue' bitPos = (head <$>) . (maximumBy (comparing length) <$>) . extract . group . sort . map (!! bitPos)
				where extract l@[x, y] = (length x == length y, l)
				      extract l	       = (False, l)

leastCommonValue :: Int -> [String] -> Char
leastCommonValue bitPos = \case { '0' -> '1'; _ -> '0' } . mostCommonValue bitPos 

filterByMostCommon :: Int -> [String] -> [String]
filterByMostCommon bitPos es = filterEntries bitPos (==mode) es
				 where mode = mostCommonValue bitPos es

filterByLeastCommon bitPos es = filterEntries bitPos (==antimode) es
				where antimode = leastCommonValue bitPos es

filterDown :: (Int -> [a] -> [a]) -> [a] -> a
filterDown f xs = head . snd $ until ((==1) . length . snd) update (0, xs) 			
			where update (n, ys) = (n+1, f n ys)

toDenary :: String -> Int
toDenary s = fst $ foldl' f (0, 2^(length s - 1)) s
		where f (n, mult) c = (if c == '1' then n + mult else n, mult `div` 2)

partTwo = ((*) `on` toDenary) oxygenGenRating co2ScrubRating
		where oxygenGenRating = filterDown filterByMostCommon entries
		      co2ScrubRating = filterDown filterByLeastCommon entries
