{-# LANGUAGE TemplateHaskell, TupleSections #-}
module DayFour where

import Data.FileEmbed      (embedStringFile)
import Data.List	   (transpose, find)
import Data.List.Split
import Data.Maybe

type Board = [[(Int, Bool)]]

toBoard :: [[Int]] -> Board
toBoard = map (map (,False))

inputData :: ([Int], [Board])
inputData = (map read $ splitOn "," x, map (toBoard . map toTable . splitOn "\n") $ init xs) 
		where (x:xs) = splitOn "\n\n" $(embedStringFile "assets/input.txt")
		      toTable = map read . words . unwords . words

updateMarking :: Int -> Board -> Board
updateMarking n = map (map update)
			where update (x, _) | x == n = (n, True)
			      update x      = id x

boardWins :: Board -> Bool
boardWins b = or . map (any (all snd)) $ [b, transpose b]

calcScore :: Int -> Board -> Int
calcScore v b = (*v) $ sum [n | (n, False) <- concat b]

playBingo :: ([Int], [Board]) -> Maybe Int
playBingo (x, y) = fmap (calcScore f2) $ find boardWins f1
		where (_, f1, f2) = until (\(_, q, _) -> any boardWins q) draw (x, y, 0)
		      draw (a, b, _) = (tail a, map (updateMarking $ head a) b, head a)	

-- playBingoTwo :: ([Int], [Board]) -> Int
playBingoTwo (x, y) = calcScore f2 lw
			where (ls, f1, f2, lw) = until (\(q1, q2, _, _) -> or [null q1, all boardWins q1, null q2]) ((\(a, b, c, d) -> (a, filter (not . boardWins) b, c, getLastWinning b d)) . draw) (x, y, 0, [[]])
			      draw (a, b, _, d) = (tail a, map (updateMarking $ head a) b, head a, d)

getLastWinning :: [Board] -> Board -> Board
getLastWinning bs defval = case v of 
				Nothing -> defval;
				Just newval -> newval; 
					where v = find boardWins $ reverse bs
